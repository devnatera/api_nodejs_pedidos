const express = require('express')
const controller = require('../app/http/controllers/pedido.controller')
const router = express.Router()

router.post('/', async(req, res, next) => {
  return await controller.addPedido(req, res, next)
})

module.exports = router