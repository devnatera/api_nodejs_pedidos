const pedidoController = require('../app/http/controllers/pedido.socket.controller')

function router(socket) {
  socket.io.on('connection', (socket) => {
    console.log('Nuevo cliente conectado');
    socket.emit('mensaje', 'Bienvenido!');
  
    socket.on('nuevo-pedido', (message) => {
      console.log('socket', JSON.parse(message));
      pedidoController.addPedido(JSON.parse(message))
        .then(resp => console.log(resp))
        .catch(error => console.log(error))
    })
  })
}

module.exports = router