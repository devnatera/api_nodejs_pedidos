const pedido = require('./pedido.router')

const routes = (server) => {
  server.use('/api/pedidos', pedido)
}

module.exports = routes