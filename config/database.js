const mongoose = require('mongoose')

mongoose.Promise = global.Promise

async function connect(url) {
  await mongoose.connect(url, {
    useNewUrlParser: true,
  })
}

module.exports = connect