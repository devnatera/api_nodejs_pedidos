require('dotenv').config()

const config = {
  appUrl: process.env.APP_URL || 'http://localhost',
  port: process.env.APP_PORT || 3000,
  dbUrl: process.env.DB_URL,
};

module.exports = config;