const socketIO = require('socket.io');
const socket = {};

function connect(server) {
  socket.io = socketIO(server, {
    cors: {
      origin: '*',
      // se puede comentar
      methods: ['GET', 'POST'],
      credentials: true,
      allowEIO3: true
    },
    transport: ['websocket']
  })
}

module.exports = {
  connect,
  socket,
}