const express = require('express')
const cors = require('cors')

const appConfig = require('./config/app')
const dbConfig = require('./config/database')
const router = require('./routes')
const socketRouter = require('./routes/pedido.socket.router')
const {errorHandler} = require('./app/http/middlewares/error.handler')

const app = express()
app.use(cors())
app.use(express.json())
router(app)
app.use(errorHandler)

dbConfig(appConfig.dbUrl)
  .catch(_ => console.log('Error connection database'))

app.get('/', (req, res) => {
  res.send('<h1>Server Started</h1>')
})

const server = require('http').Server(app)
const {connect, socket} = require('./socket')
connect(server)
socketRouter(socket)

server.listen(appConfig.port, () => {
  console.log(`Server listening in port ${appConfig.port}`);
})