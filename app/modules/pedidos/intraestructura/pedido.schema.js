const mongoose = require('mongoose')

const Schema = mongoose.Schema

const mySchema = new Schema({
  idCuenta: Number,
  idPedido: Number,
  producto: String,
  cantidad: Number,
  valor: Number,
  total: Number,
  cliente: Object
})

const model = mongoose.model('Pedido', mySchema)
module.exports = model