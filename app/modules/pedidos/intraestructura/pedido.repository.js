const Model = require('./pedido.schema')

function addPedido(pedido) {
  const myPedido = new Model(pedido)
  return myPedido.save()
}

module.exports = {
  add: addPedido,
}