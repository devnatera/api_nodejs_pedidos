const Joi = require('joi')

class Pedido {
  constructor({idCuenta, idPedido, producto, cantidad, valor, total, cliente}) {
    this.idCuenta = idCuenta
    this.idPedido = idPedido
    this.producto = producto
    this.cantidad = cantidad
    this.valor = valor
    this.total = total
    this.cliente = cliente
  }

  async toCreate() {
    try {
      return await toCreateData(this)
    } catch (error) {
      error.statusCode = 422
      throw error
    }
  }
}

async function toCreateData(data) {
  const schema = Joi.object({
    idCuenta:   Joi.number().required(),
    idPedido:   Joi.number().required(),
    producto:   Joi.string().required(),
    cantidad:   Joi.number().required(),
    valor:      Joi.number().required(),
    total:      Joi.number().required(),
    cliente:    Joi.object({
      nombre:     Joi.string().required(),
      email:      Joi.string().required(),
      telefono:   Joi.any().optional(),
    }).unknown().required(),
  }).unknown()

  return await schema.validateAsync(data)
}

module.exports = Pedido