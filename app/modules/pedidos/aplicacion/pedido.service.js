const pedidoEntity = require('../dominio/pedido.entity')
const repo = require('../intraestructura/pedido.repository')

async function add(data) {
  pedido = await new pedidoEntity(data).toCreate()
  return await repo.add(pedido)
}

module.exports = {
  add
}