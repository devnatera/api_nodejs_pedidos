const service = require('../../modules/pedidos/aplicacion/pedido.service')

async function addPedido(data) {
  try {
    response = await service.add(data)
    return {resp: response}
  } catch (error) {
    return {error: error.message}
  }
}

module.exports = {
  addPedido,
}