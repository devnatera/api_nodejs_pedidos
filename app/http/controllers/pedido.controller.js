const service = require('../../modules/pedidos/aplicacion/pedido.service')

async function addPedido(req, res, next) {
  try {
    response = await service.add(req.body)
    return res.json(response)
  } catch (error) {
    next(error)
  }
}

module.exports = {
  addPedido,
}